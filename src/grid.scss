/**
 * @file
 * Provides a minimal but flexible and responsive css-grid based layout system.
 *
 * Based on a 48 column grid, includes classes covering all possible widths
 * based on 1/12 or 1/8 (class-names use lowest common denominator).
 *
 * 1.5Kb minified, less if served gzipped.
 *
 * Rows can be divided equally either by adding a class like `.grr--halves` to
 * a row containing an appropriate number of child divs, or by assiging each
 * child div a cell class:
 *
 * <div class="grr grr--halves">
 *   <div>1/2</div>
 *   <div>1/2</div>
 * </div>
 *
 * <div class="grr">
 *   <div class="grc--1-2">1/2</div>
 *   <div class="grc--1-2">1/2</div>
 * </div>
 *
 * The following table shows the list of available classes for basic divisions:
 *
 * | Row class      | Cell class   | Fraction | Eights | Twelfths |
 * |----------------|--------------|----------|--------| ---------|
 * | .grr--twelfths | .grc--1-12   | 1/12     | n/a    | n/a      |
 * | .grr--eighths  | .grc--1-8    | 1/8      | n/a    | n/a      |
 * | .grr--sixths   | .grc--1-6    | 1/6      | n/a    | 2/12     |
 * | .grr--fourths, |              |          |        |          |
 * | .grr--quarters | .grc--1-4    | 1/4      | 2/8    | 3/12     |
 * | .grr--thirds   | .grc--1-3    | 1/3      | n/a    | 4/12     |
 * |                | .grc--3-8    | 3/8      | n/a    | n/a      |
 * |                | .grc--5-12   | 5/12     | n/a    | n/a      |
 * | .grr--halves   | .grc--1-2    | 1/2      | 4/8    | 6/12     |
 * |                | .grc--7-12   | 7/12     | n/a    | n/a      |
 * |                | .grc--5-8    | 5/8      | n/a    | n/a      |
 * |                | .grc--2-3    | 2/3      | n/a    | 8/12     |
 * |                | .grc--3-4    | 3/4      | 6/8    | 9/12     |
 * |                | .grc--5-6    | 5/6      | n/a    | 10/12    |
 * |                | .grc--7-8    | 7/8      | n/a    | n/a      |
 * |                | .grc--11-12  | 11/12    | n/a    | n/a      |
 * | .grr-full      | .grc--full   | n/a      | 8/8    | 12/12    |
 * +----------------+--------------+----------+--------+ ---------+
 *
 * The library also includes classes for centering individual cells of a given
 * size:
 *
 * <div class="grr--centered">
 *   <div class="grc--1-2">1/2</div>
 * </div>
 *
 * Since the rows are css-grid based, it's simple to build more complex layouts
 * when needed. For example:
 *
 * Desired layout:
 *
 * |-----------------------------------------------------------|
 * |                         Column #                          |
 * |----+----+----+----+----+----+----+----+----+----+----+----|
 * | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 10 | 11 | 12 |
 * |----+----+----+----+----+----+----+----+----+----+----+----|
 * |    |    |    |    |    |    |    |    |    |    |    |    |
 * |    |    |    |    |    |    |    |    |    |    |    |    |
 * v    v    v    v    v    v    v    v    v    v    v    v    v
 *
 * +--------------+--------------+--------------+--------------+
 * |xxxxxxxxxxxxxx|      1/4     |             1/2             |
 * +--------------+-----------------------------+--------------+
 * |             1/2             |      1/4     |xxxxxxxxxxxxxx|
 * +-----------------------------+--------------+--------------+
 *
 * HTML
 *
 * <div class="grct">
 *   <div class="grr--halves gr__debug--row gr__debug--cells">
 *     <div>1/2</div>
 *     <div class="grc--custom-1">Custom 1</div>
 *     <div>1/2</div>
 *     <div class="grc--custom-2">Custom 2</div>
 *   </div>
 * </div>
 *
 * CSS
 *
 * .grc--custom-1 { grid-area: 1/13/1/25; }
 * .grc--custom-2 { grid-area: 2/25/2/37; }
 *
 * Notes:
 *
 * - It would make sense to determine at least the 'grid-column' values (and
 *   maybe also the 'grid-area' values using calc(), but we don't for two
 *   reasons:
 *
 *   1. Chromium-based browsers seem unable to cope with some instances of
 *      nested calc(), so caution is warranted,
 *   2. The file size would increase quite a bit, and the goal is < 2Kb.
 *
 *   See also the repository's demo page.
 */

@use "sass:math";
@import "../node_modules/breakpoint-sass/stylesheets/breakpoint";
@import "functions";

/* Variables: SASS */

$grid-columns: 48 !default;
$grid-gap: 0.75em !default;
$breakpoint-small: 600px !default;
$breakpoint-medium: 900px !default;
$breakpoint-large: 1200px !default;
$small-grids-fraction: 2 !default;

/* Container */

.grct {
  --gr-cols: 48;
  --gr-gap: #{math.div($grid-gap, $small-grids-fraction)};
}

/* Rows */

.grr,
[class^="grr--"] {
  display: grid;
  grid-auto-rows: auto;
  grid-column-gap: 0;
  grid-row-gap: var(--gr-gap);
  grid-template-columns: repeat(var(--gr-cols), 1fr);
}

/* Cells: full */

[class^="grr--"] > *,
.grr--full > *,
[class^="grc--"],
.grc--full {
  grid-column: span var(--gr-cols);
  grid-column-gap: 0;
}

/**
 * Up until now, the grid has been a column of single cells.
 *
 * At this breakpoint, we start displaying the grid, but we've arbitrarily
 * decided that twelfths, eighths, and sixths are too small here. Consequenly,
 * we render them according to this rule:
 *
 *   - 1/n where n is divisible by four and NOT three (so, n = 8) is rendered as
 *     1/4 of a row.
 *   - 1/n where n is divisible by three and (so n = 6 or n = 12) is rendered
 *     as 1/3 of a row.
 *   - n-1/n where n is 6 or 12 is rendered as 2/3 of a row to fit well
 *     with 1/6 and 1/12 cells, while n-1/n where n is 8 is rendered as 3/4 of
 *     a row to fit well with 1/8 cells.
 *   - (x > 1)/n where n is divisible by three is rendered as 2/3 of a row when
 *     x > n/2, and as 1/3 of a row when x < n/2.
 */
@include breakpoint($breakpoint-small) {
  /* Rows */

  .grr,
  [class^="grr--"] {
    grid-gap: var(--gr-gap);
  }

  /* Cells: twelfths (rendered as thirds) */

  .grr--twelfths > *,
  .grc--1-12 {
    grid-column: span get-fraction-columns(1, 3);
  }

  .grr--centered > .grc--1-12 {
    grid-area: get-grid-area(1, 3);
  }

  .grc--5-12 {
    grid-column: span get-fraction-columns(1, 3);
  }

  .grr--centered > .grc--5-12 {
    grid-area: get-grid-area(1, 3);
  }

  .grc--7-12 {
    grid-column: span get-fraction-columns(2, 3);
  }

  .grr--centered > .grc--7-12 {
    grid-area: get-grid-area(2, 3);
  }

  .grc--11-12 {
    grid-column: span get-fraction-columns(2, 3);
  }

  .grr--centered > .grc--11-12 {
    grid-area: get-grid-area(2, 3);
  }

  /* Cells: eighths (rendered as quarters) */

  .grr--eighths > *,
  .grc--1-8 {
    grid-column: span get-fraction-columns(1, 4);
  }

  .grr--centered > .grc--1-8 {
    grid-area: get-grid-area(1, 4);
  }

  .grc--3-8 {
    grid-column: span get-fraction-columns(1, 2);
  }

  .grr--centered > .grc--3-8 {
    grid-area: get-grid-area(1, 2);
  }

  .grc--5-8 {
    grid-column: span get-fraction-columns(1, 2);
  }

  .grr--centered > .grc--5-8 {
    grid-area: get-grid-area(1, 2);
  }

  .grc--7-8 {
    grid-column: span get-fraction-columns(3, 4);
  }

  .grr--centered > .grc--7-8 {
    grid-area: get-grid-area(3, 4);
  }

  /* Cells: sixths (rendered as thirds) */

  .grr--sixths > *,
  .grc--1-6 {
    grid-column: span get-fraction-columns(1, 3);
  }

  .grr--centered > .grc--1-6 {
    grid-area: get-grid-area(1, 3);
  }

  .grc--5-6 {
    grid-column: span get-fraction-columns(2, 3);
  }

  .grr--centered > .grc--5-6 {
    grid-area: get-grid-area(2, 3);
  }

  /* Cells: fourths/quarters */

  .grr--quarters > *,
  .grr--fourths > *,
  .grc--1-4 {
    grid-column: span get-fraction-columns(1, 4);
  }

  .grr--centered > .grc--1-4 {
    grid-area: get-grid-area(1, 4);
  }

  .grc--3-4 {
    grid-column: span get-fraction-columns(3, 4);
  }

  .grr--centered > .grc--3-4 {
    grid-area: get-grid-area(3, 4);
  }

  /* Cells: thirds */

  .grr--thirds > *,
  .grc--1-3 {
    grid-column: span get-fraction-columns(1, 3);
  }

  .grr--centered > .grc--1-3 {
    grid-area: get-grid-area(1, 3);
  }

  .grc--2-3 {
    grid-column: span get-fraction-columns(2, 3);
  }

  .grr--centered > .grc--2-3 {
    grid-area: get-grid-area(2, 3);
  }

  /* Cells: halves */

  .grr--halves > *,
  .grc--1-2 {
    grid-column: span get-fraction-columns(1, 2);
  }

  .grr--centered > .grc--1-2 {
    grid-area: get-grid-area(1, 2);
  }
}

/**
 * At this breakpoint, we can go all in on the grid. Full width cells, thirds,
 * and quarters are already defined, but here we need to create the styles for
 * 'true' twelfths, eighths, and sixths.
 */
@include breakpoint($breakpoint-medium) {
  .grct {
    --gr-cols: 48;
    --gr-gap: #{$grid-gap};
  }

  /* Cells: twelfths (rendered 'true') */

  .grr--twelfths > *,
  .grc--1-12 {
    grid-column: span get-fraction-columns(1, 12);
  }

  .grr--centered > .grc--1-12 {
    grid-area: get-grid-area(1, 12);
  }

  .grc--5-12 {
    grid-column: span get-fraction-columns(5, 12);
  }

  .grr--centered > .grc--5-12 {
    grid-area: get-grid-area(5, 12);
  }

  .grc--7-12 {
    grid-column: span get-fraction-columns(7, 12);
  }

  .grr--centered > .grc--7-12 {
    grid-area: get-grid-area(7, 12);
  }

  .grc--11-12 {
    grid-column: span get-fraction-columns(11, 12);
  }

  .grr--centered > .grc--11-12 {
    grid-area: get-grid-area(11, 12);
  }

  /* Cells: eighths (rendered 'true') */

  .grr--eighths > *,
  .grc--1-8 {
    grid-column: span get-fraction-columns(1, 8);
  }

  .grr--centered > .grc--1-8 {
    grid-area: get-grid-area(1, 8);
  }

  .grc--3-8 {
    grid-column: span get-fraction-columns(3, 8);
  }

  .grr--centered > .grc--3-8 {
    grid-area: get-grid-area(3, 8);
  }

  .grc--5-8 {
    grid-column: span get-fraction-columns(5, 8);
  }

  .grr--centered > .grc--5-8 {
    grid-area: get-grid-area(5, 8);
  }

  .grc--7-8 {
    grid-column: span get-fraction-columns(7, 8);
  }

  .grr--centered > .grc--7-8 {
    grid-area: get-grid-area(7, 8);
  }

  /* Cells: sixths (rendered 'true') */

  .grr--sixths > *,
  .grc--1-6 {
    grid-column: span get-fraction-columns(1, 6);
  }

  .grr--centered > .grc--1-6 {
    grid-area: get-grid-area(1, 6);
  }

  .grc--5-6 {
    grid-column: span get-fraction-columns(5, 6);
  }

  .grr--centered > .grc--5-6 {
    grid-area: get-grid-area(5, 6);
  }
}
